﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Laba4.Models
{
    public class ImageModel
    {
        public string Description { get; set; }
        public string ImageSource { get; set; }

        public static IEnumerable<ImageModel> GetDocs()
        {
            //string rootPath = "Assets/Imgs";
            //var dir = new DirectoryInfo(rootPath);
            //return dir.GetFiles().Select(t => new ImageModel() { ImageSource = $"{rootPath}/{t.Name}"});

            using (var sr = new StreamReader(@"Assets\ImgsInfo.json"))
            {
                var a = JsonConvert.DeserializeObject<IEnumerable<ImageModel>>(sr.ReadToEnd());
                return a;
            }
        }
    }
}
