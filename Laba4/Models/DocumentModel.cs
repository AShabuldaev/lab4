﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Laba4.Models
{
    public class DocumentModel
    {
        public string Name { get; set; }

        public void OpenInWord()
        {

        }

        public static IEnumerable<DocumentModel> GetImgs()
        {
            string rootPath = "Assets/Docs";
            var dir = new DirectoryInfo(rootPath);
            return dir.GetFiles().Select(t => new DocumentModel() { Name = t.Name });
        } 
    }
}
