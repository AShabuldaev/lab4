1
00:00:13,000 --> 00:00:22,000
"Mobile thermodestruction installation TDU-Factor 2000 Technology:
Step 1: Sludge Discharge"

2
00:00:24,000 --> 00:00:30,000
"Charging hopper with screw or waste chute system"

3
00:00:33,000 --> 00:00:36,000
"Step 2: burning sludge"

4
00:00:39,000 --> 00:00:46,000
"Diesel, gas, fuel oil or waste oil burner, optional"

5
00:00:47,000 --> 00:00:52,000
"Combustion drum with temperature up to 900 degrees Celsius"

6
00:00:55,000 --> 00:01:00,000
"Manual or automatic ash discharge, optional"

7
00:01:02,000 --> 00:01:06,000
"Step 3: forced flue gas afterburning"

8
00:01:09,000 --> 00:01:15,000
"Flue gas afterburner"

9
00:01:18,000 --> 00:01:23,000
"Step 4 - Purification of exhaust gases from heavy
particles and dust"

10
00:01:25,000 --> 00:01:28,000
"Cyclone Dust Collectors"

11
00:01:31,000 --> 00:01:37,000
"Noise level during operation no more than 80 dB"