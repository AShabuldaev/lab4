﻿using Laba4.Models;
using System;
using System.Collections.ObjectModel;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Laba4
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private readonly ObservableCollection<DocumentModel> _docs;

        private readonly ObservableCollection<ImageModel> _imgs;
        public MainPage()
        {
            this.InitializeComponent();
            //_docs = new ObservableCollection<DocumentModel>(DocumentModel.GetImgs());
            //_imgs = new ObservableCollection<ImageModel>(ImageModel.GetDocs());
            //DocLV.ItemsSource = _docs;
            //ImgsFV.ItemsSource = _imgs;
        }

        private async void HyperlinkButton_Click(object sender, RoutedEventArgs e)
        {
            if(sender is HyperlinkButton hb)
            {
                var picker = new FileOpenPicker();

                picker.FileTypeFilter.Add(".docx");
                picker.FileTypeFilter.Add(".doc");

                picker.SuggestedStartLocation = PickerLocationId.DocumentsLibrary;

                var file = await picker.PickSingleFileAsync();

                await Windows.System.Launcher.LaunchFileAsync(file);
            }
        }
    }
}
